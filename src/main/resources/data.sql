INSERT INTO parent(parent_id, unique_identifier, parent_name)
VALUES
    ('parent-uuid-1', '172.17.0.1', 'parent-1'),
    ('parent-uuid-2', '172.17.0.2', 'parent-2'),
    ('parent-uuid-3', '172.17.0.3', 'parent-3');

INSERT INTO child(id, child_name, parent_id)
VALUES
    (1, 'child-1', 'parent-uuid-1'),
    (2, 'child-2', 'parent-uuid-2');

INSERT INTO cousin(id, cousin_name, parent_id)
VALUES
    (1, 'cousin-1', 'parent-uuid-1'),
    (2, 'cousin-2', 'parent-uuid-1'),
    (3, 'cousin-3', 'parent-uuid-1'),
    (4, 'cousin-4', 'parent-uuid-3'),
    (5, 'cousin-5', 'parent-uuid-3'),
    (6, 'cousin-6', 'parent-uuid-3');
