create table child (id integer not null, child_name varchar(255), parent_id varchar(64), primary key (id));
create table cousin (id integer not null, cousin_name varchar(255), parent_id varchar(64), primary key (id));
create table parent (parent_id varchar(64) not null, parent_name varchar(255) unique, unique_identifier varchar(20), primary key (parent_id));
create sequence hibernate_sequence start with 1 increment by 1;
alter table child add constraint FK7dag1cncltpyhoc2mbwka356h foreign key (parent_id) references parent;
alter table cousin add constraint FKs22ig3w31ajqi7vs4qytob6k8 foreign key (parent_id) references parent;