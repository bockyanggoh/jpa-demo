package com.example.demo.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ParentRepo extends JpaRepository<Parent, String> {


    Optional<Parent> findParentByUniqueIpIs(String ip);

    @Query("SELECT DISTINCT p.uuid from Parent p where p.uniqueIp = ?1")
    String findParentIdWithUniqueIp(String ip);

    @Query(nativeQuery = true, value = "select * from parent p\n" +
            "  inner join child c on c.parent_id = p.parent_id\n" +
            "  inner join cousin cs on cs.parent_id = p.parent_id\n" +
            "  where p.parent_id = (select p.uuid = ?1)")
    Optional<Parent> findParentsWithChildAndCousins(String uuid);
}
