package com.example.demo.jpa;

import com.google.gson.Gson;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Cousin {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Parent parent;

    @Column(name = "cousin_name")
    private String cousinName;

    public Cousin(Parent parent, String cousinName) {
        this.parent = parent;
        this.cousinName = cousinName;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
