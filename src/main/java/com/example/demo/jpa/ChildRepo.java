package com.example.demo.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChildRepo extends JpaRepository<Child, Integer> {

    @Query("SELECT c from Child c " +
            "INNER JOIN FETCH c.parent p WHERE p.uniqueIp = ?1")
    Optional<Child> findChild(String ip);
}
