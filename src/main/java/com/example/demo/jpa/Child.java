package com.example.demo.jpa;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Child {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "parent_id")
    private Parent parent;

    @Column(name = "child_name")
    private String childName;

    public Child(Parent parent, String childName) {
        this.parent = parent;
        this.childName = childName;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
