package com.example.demo.jpa;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Parent {

    @Id
    @Column(name = "parent_id", length = 64)
    private String uuid;

    @Column(name = "unique_identifier", length = 20)
    private String uniqueIp;

    @Column(name = "parent_name")
    private String parentName;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Cousin> cousins = new ArrayList<>();

    @OneToOne(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Child child;

    public Parent(String uuid, String uniqueIp, String parentName) {
        this.uuid = uuid;
        this.uniqueIp = uniqueIp;
        this.parentName = parentName;
    }

    public void addCousin(Cousin cousin){
        if(this.cousins == null){
            this.cousins = new ArrayList<>();
        }
        this.cousins.add(cousin);
    }

    public void removeAllCousins(){
        if(this.cousins != null)
            this.cousins.clear();
    }

    public void removeCousin(Cousin cousin){
        if(this.cousins != null)
            this.cousins.remove(cousin);

    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
