package com.example.demo;

import com.example.demo.jpa.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Arrays;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class JpaLogicTests {

    @Autowired
    private ParentRepo parentRepo;

    @Autowired
    private ChildRepo childRepo;

    private static boolean setUpIsDone = false;

    final String uniqueIp = "172.17.0.12";

//    @Before
//    public void loadDataIntoDB(){
//        if(setUpIsDone == false){
//            Parent parent = null;
//            for(int i = 10; i <= 15; i++){
//                parent = new Parent("random-string-"+i, "172.17.0."+i, "parent_1");
//                Child child = new Child(parent ,"child-"+i );
//                parent.setChild(child);
//                Cousin cousin = new Cousin(parent, "cousin-1");
//                Cousin cousin2 = new Cousin(parent, "cousin-2");
//                Cousin cousin3 = new Cousin(parent, "cousin-3");
//                parent.setCousins(Arrays.asList(cousin, cousin2, cousin3));
//                parentRepo.save(parent);
//            }
//            parentRepo.flush();
//            setUpIsDone = true;
//        }
//    }

    @Test
    public void retrieveParentSuccessTest() {
        Assert.assertTrue(parentRepo.findParentByUniqueIpIs(uniqueIp).isPresent());
    }

    @Test
    public void retrieveChildSuccessTest() {
        Child child = childRepo.findChild(uniqueIp).get();

        Assert.assertThat(child.getParent().getUniqueIp(), is(uniqueIp));
        Assert.assertThat(child.getChildName(), is("child-12"));
    }

    @Test
    @Transactional
    public void retrieveCousinSuccessTest() {
        Parent parent = parentRepo.findParentByUniqueIpIs(uniqueIp).get();
        Assert.assertThat(parent.getUniqueIp(), is(uniqueIp));
        Assert.assertThat(parent.getChild().getChildName(), is("child-12"));
        if(parent.getCousins().size() == 0){
            Assert.assertTrue(false);
        }
        else {
            Assert.assertThat(parent.getCousins().get(0).getCousinName(), is("cousin-1"));
        }
    }

    @Test
    @Transactional
    public void changeChildAndParentSuccessTest(){
        Parent parent = parentRepo.findParentByUniqueIpIs(uniqueIp).get();
        parent.getChild().setChildName("changed-"+uniqueIp);
        parent.setParentName("changed-"+uniqueIp);
        parentRepo.saveAndFlush(parent);
        Parent verify = parentRepo.findParentByUniqueIpIs(uniqueIp).get();
        log.info(verify.getParentName());
        Assert.assertThat(verify.getParentName(), is("changed-"+uniqueIp));
        Assert.assertThat(verify.getChild().getChildName(), is("changed-"+uniqueIp));
    }

    @Test
    @Transactional
    public void changeChildAndParentAndInsertCousinSuccessTest(){
        Parent parent = parentRepo.findParentByUniqueIpIs(uniqueIp).get();
        parent.getChild().setChildName("changed-"+uniqueIp);
        parent.setParentName("changed-"+uniqueIp);
        parent.addCousin(new Cousin(parent, "changed-"+uniqueIp));
        parentRepo.saveAndFlush(parent);
        Parent verify = parentRepo.findParentByUniqueIpIs(uniqueIp).get();
        log.info(verify.getParentName());
        Assert.assertThat(verify.getParentName(), is("changed-"+uniqueIp));
        Assert.assertThat(verify.getChild().getChildName(), is("changed-"+uniqueIp));
        Boolean foundFlag = false;
        for(Cousin cousin: parent.getCousins()){
            if(cousin.getCousinName().equals("changed-"+uniqueIp)){
                foundFlag = true;
            }
        }
        Assert.assertTrue(foundFlag);
    }

    @Test
    @Transactional
    public void changeChildAndParentAndChangeCousinSuccessTest(){
        Parent parent = parentRepo.findParentByUniqueIpIs(uniqueIp).get();
        parent.getChild().setChildName("changed-"+uniqueIp);
        parent.setParentName("changed-"+uniqueIp);
        for(Cousin cousin: parent.getCousins()){
            if(cousin.getCousinName().equals("cousin-1"))
                cousin.setCousinName("changed-cousin-1");
        }
        parentRepo.saveAndFlush(parent);
        Parent verify = parentRepo.findParentByUniqueIpIs(uniqueIp).get();
        log.info(verify.getParentName());
        Assert.assertThat(verify.getParentName(), is("changed-"+uniqueIp));
        Assert.assertThat(verify.getChild().getChildName(), is("changed-"+uniqueIp));
        Boolean foundFlag = false;
        for(Cousin cousin: parent.getCousins()){
            if(cousin.getCousinName().equals("changed-cousin-1")){
                foundFlag = true;
            }
        }
        Assert.assertTrue(foundFlag);
    }


}
